package qubit

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.time.Instant
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.collections.HashMap

val mapper = ObjectMapper()

fun aggregateJson(jsonStream: List<String>): Map<String, Map<String, Int>> =
    calculateTotals(
        groupRequestIdsByUrlAndDate(jsonStream)
    )

private fun groupRequestIdsByUrlAndDate(jsonStream: List<String>): Map<String, Map<String, Set<String>>> {
    val uniqueIds: MutableMap<String, MutableMap<String, MutableSet<String>>> = HashMap()
    jsonStream.forEach {
        try {
            val jsonMap: Map<String, String> = mapper.readValue(it, object: TypeReference<HashMap<String, String>>() {})
            val date = epochToDate(jsonMap["timestamp"]!!)
            val url = jsonMap["url"]!!
            val userId = jsonMap["userid"]!!

            val urlData = uniqueIds.getOrDefault(url , HashMap())
            val dateIds = urlData.getOrDefault(date, HashSet())
            dateIds.add(userId)
            urlData[date] = dateIds
            uniqueIds[url] = urlData
        } catch (e: Exception) {
            print(e.message)
        }
    }
    return uniqueIds
}

private fun epochToDate(timestamp: String): String =
    LocalDateTime.ofInstant(
        Instant.ofEpochMilli(timestamp.toLong()),
        TimeZone.getDefault().toZoneId()
    ).format(
        DateTimeFormatter.ofPattern("yyyy-MM-dd")
    )

private fun calculateTotals(uniqueIds: Map<String, Map<String, Set<String>>>): Map<String, Map<String, Int>> =
    uniqueIds.map{
        url -> url.key to url.value.map {
            date -> date.key to date.value.size
        }.toMap()
    }.toMap()
