package qubit

import org.junit.jupiter.api.Test
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import kotlin.test.assertEquals


class JsonAggregatorTest {

    private val exampleJson =
        """
        {
            "userid": "b20524ae-befe-11e3-b335-425861b86ab6",
            "url":"http://www.someamazingwebsite.com/county-2014/engine/match/692721.html?cluster=undefined;view=comparison;wrappertype=none#live",
            "type": "GET",
            "timestamp": 1360662163000
        }
        """

    @Test
    fun Parses_example_json() {
        assertEquals(
            mapOf(
            "http://www.someamazingwebsite.com/county-2014/engine/match/692721.html?cluster=undefined;view=comparison;wrappertype=none#live" to
                    mapOf(
                        "2013-02-12" to 1
                    )
            ),
            aggregateJson(listOf(
                exampleJson
            ))
        )
    }

    @Test
    fun Ignores_invalid_json() {
        assertEquals(
            mapOf(
                "someUrl" to
                        mapOf(
                            "2013-02-12" to 1
                        )
            ),
            aggregateJson(listOf(
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "2013-02-12"
                ),
                "{\"userId\" : \"hello\"}",
                "{\"userId\" : asdhialgagl&*()%^7asak"
            ))
        )
    }

    @Test
    fun Sums_up_values_when_ids_are_unique() {
        assertEquals(
            mapOf(
                "someUrl" to mapOf("1995-07-10" to 3)
            ),
            aggregateJson(listOf(
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "1995-07-10"
                ),
                generateJson(
                    userId = "2",
                    url = "someUrl",
                    date = "1995-07-10"
                ),
                generateJson(
                    userId = "3",
                    url = "someUrl",
                    date = "1995-07-10"
                )
            ))
        )
    }

    @Test
    fun Does_not_increase_counts_when_ids_are_the_same() {
        assertEquals(
            mapOf(
                "someUrl" to mapOf("1995-07-10" to 1)
            ),
            aggregateJson(listOf(
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "1995-07-10"
                ),
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "1995-07-10"
                ),
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "1995-07-10"
                )
            ))
        )
    }

    @Test
    fun Splits_ids_by_date() {
        assertEquals(
            mapOf(
                "someUrl" to
                    mapOf(
                        "1995-07-10" to 1,
                        "1995-07-12" to 1
                    )
            ),
            aggregateJson(listOf(
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "1995-07-10"
                ),
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "1995-07-12"
                )
            ))
        )
    }

    @Test
    fun Splits_ids_by_url() {
        assertEquals(
            mapOf(
                "someUrl" to mapOf("1995-07-10" to 1),
                "anotherUrl" to mapOf("1995-07-10" to 1)
            ),
            aggregateJson(listOf(
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "1995-07-10"
                ),
                generateJson(
                    userId = "1",
                    url = "anotherUrl",
                    date = "1995-07-10"
                )
            ))
        )
    }

    @Test
    fun Correctly_splits_ids_by_url_and_date() {
        assertEquals(
            mapOf(
                "someUrl" to
                    mapOf(
                        "1995-07-10" to 2,
                        "2021-01-01" to 1
                    ),
                "anotherUrl" to
                    mapOf(
                        "1995-07-12" to 1,
                        "2021-07-04" to 1
                    )
            ),
            aggregateJson(listOf(
                generateJson(
                    userId = "1",
                    url = "someUrl",
                    date = "1995-07-10"
                ),
                generateJson(
                    userId = "2",
                    url = "someUrl",
                    date = "1995-07-10"
                ),
                generateJson(
                    userId = "3",
                    url = "someUrl",
                    date = "2021-01-01"
                ),
                generateJson(
                    userId = "4",
                    url = "anotherUrl",
                    date = "1995-07-12"
                ),
                generateJson(
                    userId = "5",
                    url = "anotherUrl",
                    date = "2021-07-04"
                )
            ))
        )
    }

    private fun generateJson(userId: String, url: String, date: String, type: String = "GET") =
        """
        {
            "userid": "$userId",
            "url": "$url",
            "type": "$type",
            "timestamp": ${dateToTimestamp(date)}
        }
        """

    private fun dateToTimestamp(date: String): String =
        LocalDateTime.of(
            LocalDate.parse(
                date,
                DateTimeFormatter.ofPattern("yyyy-MM-dd")
            ),
            LocalTime.MIN
        )
            .toInstant(ZoneOffset.UTC)
            .toEpochMilli()
            .toString()
}