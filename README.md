# Qubit Json Aggregation take home test

This is a utility I wrote for aggregating JSON containing info on http requests received by servers.

## Running the code

This is a Kotlin project that targets Java 11 and uses Gradle to build.

This code does not have a main method as it is intended to be used as a library. In production, I imagine this running
behind an HTTP endpoint that takes batches of JSON in via a POST, aggregates them and sends them off to storage with
another POST request. Initial tech stack would be a Kotlin process that configures a Netty Server using KTOR. It should
be run with a JAR file that is built using Gradle.

The unit tests can be run with the following command:

```./gradlew test```

## Scalability

### Time Complexity

I believe that the time complexity of this is O(N). With the most expensive operation being building the map of maps of
sets of unique IDs. This should make it fairly scalable.

### Scaling up to multiple active nodes

Under load that is too large to be handled by a single node, another microservice would be needed to ensure that it
scaled. This "Total Aggregator" service would take the resulting maps from this service and combine them into the sum of
both maps. Combining these maps should be also be doable in O(N) time.

Should the Total Aggregator node become a bottleneck, it's load could be divided into multiple nodes which feed their
results into another Total Aggregator. This pyramid of mergers could be built up as far as is required to meet load.

Between each layer of nodes should be a load balancer that uses a hash ring to dynamically balance requests between
nodes. Depending on the value to the business of not losing packets of data, parallel pipelines could be run at each
step to ensure node failure does not cause packets of data to be lost. Autoscaling could be achieved using a
containerization service like kubernetes which could scale up the mergers to meet demand. Although due to the stateless
nature of this service, a serverless architecture could be preferable if this process is running in the cloud.

If autoscaling doesn't prove responsive enough to handle spikes in traffic, the loadbalancers should be replaced with
a distributed queueing system. The industry standard for this would be Apache Kafka. Total Packets could be put on the
Kafka with a topic that corresponds to the number of times they have been totalled. Total Aggregators would then need to
be tweaked to claim batches of requests from Kafka before inserting the result on the next "tier" up.